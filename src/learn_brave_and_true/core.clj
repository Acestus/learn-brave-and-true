(ns cloud-noob.core
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))
(+ 1 1 1)
(str "It was the panda " "in the library " "with a dust buster")
(if true "By Zeus's hammer!" "By Aquaman's trident!")
(if false"By Zeus's hammer!""By Aquaman's trident!")
(if false "By Odin's Elbow!")
(if true
(do (println "Success!")
"By Zeus's hammer!")
(do (println "Failure!")
"By Aquaman's trident!"))
(when true
(println "Success!")
"abra cadabra")
(if nil
"This won't be the result because nil is falsey"
"nil is falsey")
(nil? 1)
(= 1 1)
(= nil nil)
(or false nil :large_I_mean_venti :why_cant_I_just_say_large)
(or (= 0 1) (= "yes" "no"))
(or nil)
(and :free_wifi :hot_coffee)
(and :feelin_super_cool nil false)
(defn error-message
  [severity]
  (str "OH GOD! IT'S A DISASTER! WE'RE "
       (if (= severity :mild)
         "MILDLY INCONVENIENCED!"
         "DOOOOOOOMED!")))

(get {:a 0 :b 1} :b)
(get {:a 0 :b {:c "ho hum"}} :b)
(get {:a 0 :b 1} :c "unicorns?")
(get {:a 0 :b 1} :c)
(get-in {:a 0 :b {:c "ho hum"}} [:b :c])
({:name "The Human Coffeepot"} :name)
(:a {:a 1 :b 2 :c 3}) 
(get {:a 1 :b 2 :c 3} :a)
(:d {:a 1 :b 2 :c 3} "No gnome knows homes like Noah knows")
(get [3 2 1] 0)
(get ["a" {:name "Pugsley Winterbottom"} "c"] 1)
(vector "creepy" "full" "moon")
(conj [1 2 3] 4)
'(1 2 3 4)
(nth '(:a :b :c) 0)
(nth '(:a :b :c) 2)
(list 1 "two" {3 4})
(conj '(1 2 3) 4)
(hash-set 1 1 2 2)
#{"kurt vonnegut" 20 :icicle}
(conj #{:a :b} :b)
(set [3 3 3 4 4])
(contains? #{:a :b} :)
(:a #{:a :b})
(get #{:a :b} :a)
(or + -)
(= 1 1)
((and (= 1 1) +) 1 2 3)
(inc 1.1)
(map inc [0 1 2 3])
(+ (inc 199) (/ 100 (- 7 2)))

➊ (defn too-enthusiastic
"Return a cheer that might be a bit too enthusiastic"
[name]
(str "OH. MY. GOD! " name " YOU ARE MOST DEFINITELY LIKE THE BEST "
"MAN SLASH WOMAN EVER I LOVE YOU AND WE SHOULD RUN AWAY SOMEWHERE"))
(doc map)
(defn x-chop
"Describe the kind of chop you're inflicting on someone"
([name chop-type]
(str "I " chop-type " chop " name "! Take that!"))
([name]
(x-chop name "karate")))


(def sum #(reduce + %))
(def avg #(/ (sum %) (count %)))
(defn stats
  [numbers]
  (map #(% numbers) [sum count avg]))
(stats [3 4 10])
(stats [80 1 44 13 6])

(def identities
  [{:alias "Batman" :real "Bruce Wayne"}
   {:alias "Spider-Man" :real "Peter Parker"}
   {:alias "Santa" :real "Your mom"}
   {:alias "Easter Bunny" :real "Your mom"}])

(map :real identities)



(reduce (fn [new-map [key val]]
          (assoc new-map key (inc val)))

(reduce (fn [new-map [key val]]
          (if (> val 4)
            (assoc new-map key val)
            new-map))
        {}
        {:human 4.1
         :critter 3.9})


(reduce (fn [new-map [key val]]
          (assoc new-map key (inc val)))

          {}
          {:max 30 :min 10})

(take 3 [1 2 3 4 5 6 7 8 9 10])

(drop 3 [1 2 3 4 5 6 7 8 9 10])

(def food-journal
  [{:month 1 :day 1 :human 5.3 :critter 2.3}
   {:month 1 :day 2 :human 5.1 :critter 2.0}
   {:month 2 :day 1 :human 4.9 :critter 2.1}
   {:month 2 :day 2 :human 5.0 :critter 2.5}
   {:month 3 :day 1 :human 4.2 :critter 3.3}
   {:month 3 :day 2 :human 4.0 :critter 3.8}
   {:month 4 :day 1 :human 3.7 :critter 3.9}
   {:month 4 :day 2 :human 3.7 :critter 3.6}])


(take-while #(< (:month %) 3) food-journal)


(drop-while #(< (:month %) 3) food-journal)


(take-while #(< (:month %) 4)
            (drop-while #(< (:month %) 2) food-journal))

(filter #(< (:human %) 5) food-journal)

(some #(> (:critter %) 5) food-journal)
(some #(> (:critter %) 3) food-journal)

(some #(and (> (:critter %) 3) %) food-journal)

(sort [3 1 2])
(sort-by count ["aaa" "c" "bb"])

(concat [1 2] [3 4])
